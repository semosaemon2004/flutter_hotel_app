import 'package:flutter/material.dart';

class AgeOfChildWidget extends StatelessWidget {
  final int age;
  final String title;

  const AgeOfChildWidget({
    Key? key,
    required this.age,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title:  Text(title),
      trailing: Text('$age years', style:const TextStyle(
        fontSize: 15,
        color: Colors.grey
      ),),
    );
  }
}
