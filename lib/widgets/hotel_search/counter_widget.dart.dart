import 'package:flutter/material.dart';

class CounterWidget extends StatelessWidget {
  final String title;
  final int value;
  final VoidCallback? onIncrement;
  final VoidCallback? onDecrement;
  final bool canIncrement; // This parameter is required
  final bool canDecrement; // This parameter is required

  const CounterWidget({
    Key? key,
    required this.title,
    required this.value,
    this.onIncrement,
    this.onDecrement,
    required this.canIncrement,
    required this.canDecrement,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title, style: const TextStyle(fontSize: 18)),
        Row(
          children: [
            CircleAvatar(
              backgroundColor: Colors.grey.shade200,
              child: IconButton(
                icon: const Icon(Icons.remove),
                color: canDecrement ? Colors.black : Colors.grey.shade600,
                onPressed: onDecrement,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child:
                  Text(value.toString(), style: const TextStyle(fontSize: 18)),
            ),
            CircleAvatar(
              backgroundColor: Colors.grey.shade200,
              child: IconButton(
                icon: const Icon(Icons.add),
                color: canIncrement ? Colors.black : Colors.grey.shade600,
                onPressed: onIncrement,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
