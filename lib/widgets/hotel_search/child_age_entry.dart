import 'package:flutter/material.dart';

class ChildAgeEntry extends StatelessWidget {
  final int age;
  final Function(int) onAgeChanged;

  const ChildAgeEntry({
    Key? key,
    required this.age,
    required this.onAgeChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text('Age of child $age', style: const TextStyle(fontSize: 18)),
        Row(
          children: [
            IconButton(
              icon: const Icon(Icons.remove),
              onPressed: () => onAgeChanged(age - 1),
            ),
            Text('$age years', style: const TextStyle(fontSize: 18)),
            IconButton(
              icon: const Icon(Icons.add),
              onPressed: () => onAgeChanged(age + 1),
            ),
          ],
        ),
      ],
    );
  }
}
