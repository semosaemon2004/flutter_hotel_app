import 'package:flutter/material.dart';

class DateRangePicker extends StatelessWidget {
  final DateTimeRange? dateRange;
  final Function(BuildContext context) onDateRangeSelected;

  const DateRangePicker({
    Key? key,
    this.dateRange,
    required this.onDateRangeSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 50,
        width: 350,
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 4,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.transparent,
            shadowColor: Colors.transparent,
          ),
          onPressed: () => onDateRangeSelected(context),
          child: Text(
            dateRange == null
                ? 'Pick Date Range'
                : '${dateRange!.start.toString().split(' ')[0]} ==> ${dateRange!.end.toString().split(' ')[0]}',
            style: const TextStyle(color: Colors.blue),
          ),
        ),
      ),
    );
  }
}
