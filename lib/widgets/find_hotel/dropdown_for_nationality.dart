import 'package:flutter/material.dart';

class DropdownForNationality extends StatelessWidget {
  final String? selectedNationality;
  final List<String> nationalities;
  final Function(String?) onNationalityChanged;

  const DropdownForNationality({
    Key? key,
    this.selectedNationality,
    required this.nationalities,
    required this.onNationalityChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 50,
        padding: const EdgeInsets.only(left: 45),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 4,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: DropdownButtonFormField<String>(
          value: selectedNationality,
          decoration: const InputDecoration(
            labelText: 'Select Nationality',
            labelStyle: TextStyle(color: Colors.blue),
            border: InputBorder.none,
            contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          ),
          onChanged: onNationalityChanged,
          items: nationalities.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}
