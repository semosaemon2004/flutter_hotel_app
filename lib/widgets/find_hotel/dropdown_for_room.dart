import 'package:flutter/material.dart';

class DropdownForRoom extends StatelessWidget {
  final String? selectedRoom;
  final List<String> rooms;
  final Function(String?) onRoomChanged;

  const DropdownForRoom({
    Key? key,
    this.selectedRoom,
    required this.rooms,
    required this.onRoomChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 50,
        padding: const EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 4,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: DropdownButtonFormField<String>(
          value: selectedRoom,
          decoration: const InputDecoration(
            labelText: '1 Room, 2 Adult, 0 Children',
            border: InputBorder.none,
            labelStyle: TextStyle(color: Colors.blue),
            contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          ),
          onChanged: onRoomChanged,
          items: rooms.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}
