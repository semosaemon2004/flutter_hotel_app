import 'package:flutter/material.dart';
import 'package:flutter_form_app/screens/hotel_search.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Hotel Search',
      home: HotelBookingScreen(),
    );
  }
}
