class RoomDetails {
  int numberOfAdults;
  int numberOfChildren;
  List<int> childrenAges;

  RoomDetails({
    required this.numberOfAdults,
    required this.numberOfChildren,
    required this.childrenAges,
  });
}
