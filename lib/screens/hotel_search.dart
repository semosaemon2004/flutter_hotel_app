import 'package:flutter/material.dart';
import 'package:flutter_form_app/screens/find_hotel.dart';
import 'package:flutter_form_app/widgets/find_hotel/date_range_picker.dart';
import 'package:flutter_form_app/widgets/find_hotel/diagonal_clipper.dart';
import 'package:flutter_form_app/widgets/find_hotel/dropdown_for_nationality.dart';
import 'package:flutter_form_app/widgets/find_hotel/dropdown_for_room.dart';
import 'package:flutter_form_app/widgets/find_hotel/find_hotels_button.dart';
import 'package:flutter_form_app/widgets/find_hotel/input_field.dart';

class HotelBookingScreen extends StatefulWidget {
  @override
  _HotelBookingScreenState createState() => _HotelBookingScreenState();
}

class _HotelBookingScreenState extends State<HotelBookingScreen> {
  final TextEditingController _cityController = TextEditingController();
  DateTimeRange? _dateRange;
  String? _selectedNationality;
  String? _selectedRoom;

  List<String> nationalities = [
    'USA',
    'Canada',
    'UK',
    'India',
    'Others'
  ]; 

  List<String> rooms = [
    '1 Room, 2 Adult, 0 Children',
    '1 Room, 1 Adult, 0 Children',
    'More options'
  ]; 

  void selectDateRange(BuildContext context) async {
    final DateTimeRange? picked = await showDateRangePicker(
      context: context,
      firstDate: DateTime.now(),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != _dateRange)
      setState(() {
        _dateRange = picked;
      });
  }

  void _showModalBottomSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
      builder: (_) {
        return RoomsAndGuestsScreen();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
                "assets/images/background.webp"), // Replace with your image asset
            fit: BoxFit.cover,
          ),
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, bottom: 60),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(left: 10, right: 100),
                    child: ClipPath(
                      clipper: DiagonalClipper(),
                      child: Container(
                        color: Colors.blue[700],
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.only(
                            left: 20, top: 10, bottom: 10),
                        child: const Text(
                          'Hotels Search',
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            letterSpacing: 1.2,
                          ),
                        ),
                      ),
                    )),
                const SizedBox(height: 10),
                Container(
                  padding: const EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Column(
                    children: [
                      InputField(
                        text: 'Select City',
                        controller: _cityController,
                      ),
                      DateRangePicker(
                        dateRange: _dateRange,
                        onDateRangeSelected: selectDateRange,
                      ),
                      DropdownForNationality(
                        selectedNationality: _selectedNationality,
                        nationalities: nationalities,
                        onNationalityChanged: (newValue) {
                          setState(() {
                            _selectedNationality = newValue;
                          });
                        },
                      ),
                      DropdownForRoom(
                        selectedRoom: _selectedRoom,
                        rooms: rooms,
                        onRoomChanged: (newValue) {
                          setState(() {
                            _selectedRoom = newValue;
                          });
                        },
                      ),
                      FindHotelsButton(
                        onPressed: () => _showModalBottomSheet(context),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
