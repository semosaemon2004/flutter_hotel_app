import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_app/widgets/hotel_search/age_of_child_widget.dart';
import 'package:flutter_form_app/widgets/hotel_search/counter_widget.dart.dart';

class RoomsAndGuestsScreen extends StatefulWidget {
  @override
  _RoomsAndGuestsScreenState createState() => _RoomsAndGuestsScreenState();
}

class _RoomsAndGuestsScreenState extends State<RoomsAndGuestsScreen> {
  int numberOfRooms = 1;
  int numberOfAdults = 4;
  int numberOfChildren = 2;
  List<int> agesOfChildren = [14, 14]; // Assuming there are two children
  bool petFriendly = false;

  @override
  void initState() {
    super.initState();
    // Enter full-screen mode
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
  }

  @override
  void dispose() {
    // Exit full-screen mode
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 2,
        title: const Text(
          '\t\tRooms and Guests',
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontStyle: FontStyle.normal,
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.close),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CounterWidget(
                  title: 'Rooms',
                  value: numberOfRooms,
                  onIncrement: numberOfRooms < 30
                      ? () => setState(() => numberOfRooms++)
                      : null,
                  onDecrement: numberOfRooms > 1
                      ? () => setState(() => numberOfRooms--)
                      : null,
                  canIncrement: numberOfRooms < 30,
                  canDecrement: numberOfRooms > 1,
                ),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "ROOM 1",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                    ),
                    const SizedBox(height: 10),
                    CounterWidget(
                      title: 'Adults',
                      value: numberOfAdults,
                      canIncrement: numberOfAdults < 4,
                      canDecrement: numberOfAdults > 1,
                      onIncrement: () {
                        if (numberOfAdults < 4) {
                          setState(() => numberOfAdults++);
                        }
                      },
                      onDecrement: () {
                        if (numberOfAdults > 1) {
                          setState(() => numberOfAdults--);
                        }
                      },
                    ),
                    const SizedBox(height: 10),
                    CounterWidget(
                      title: 'Children',
                      value: numberOfChildren,
                      onIncrement: () {
                        setState(() {
                          numberOfChildren++;
                          agesOfChildren.add(14); // Default age for a new child
                        });
                      },
                      onDecrement: numberOfChildren > 0
                          ? () {
                              setState(() {
                                numberOfChildren--;
                                agesOfChildren.removeLast();
                              });
                            }
                          : null,
                      canIncrement: true,
                      canDecrement: numberOfChildren > 0,
                    ),
                    ...List.generate(
                        agesOfChildren.length,
                        (index) => AgeOfChildWidget(
                              age: agesOfChildren[index],
                              title: "Age of child ${index + 1}",
                            )).toList(),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: SwitchListTile(
                title: const Row(
                  children: [
                    Text('Pet-friendly'),
                    SizedBox(width: 10),
                    Icon(CupertinoIcons.info_circle), // Your desired icon
                  ],
                ),
                subtitle: const Text("Only show stays that allow pets"),
                value: petFriendly,
                onChanged: (bool value) => setState(() => petFriendly = value),
              ),
            ),
            const SizedBox(height: 200),
            Container(
              width: double.infinity,
              height: 60.0,
              child: ElevatedButton(
                onPressed: () {
                  // Handle the button press
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                  onPrimary: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                child: const Text(
                  'Apply',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
